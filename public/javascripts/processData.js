module.exports = function processData(beacons) {
	const data = new Map();
	Object.keys(beacons).forEach(key => {
		const beacon = JSON.parse(beacons[key]);
		beacon.minor = parseInt(beacon.minor);
		const minor = beacon.minor;
		data.set(minor, beacon);
	});
	return data;
};
