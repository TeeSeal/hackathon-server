const express = require('express');
const processData = require('../public/javascripts/processData');

const Actor = require('../public/javascripts/Actor');

const router = express.Router(); // eslint-disable-line

/* GET home page. */
router.get('/', (req, res) => {
	res.render('index', { title: 'Express' });
});

router.post('/', (req, res) => {
	const ip = req._remoteAddress.split('f:')[1];
	const data = processData(req.body);
	let actor;

	if (Actor.actorExists(ip)) {
		actor = Actor.getActor(ip);
	} else {
		actor = new Actor(ip, data);
	}

	actor.update(data);
	console.log(`Room ${actor.room}`);
	console.log(actor.camera);
	res.send('Got data successfuly.');
});

module.exports = router;
