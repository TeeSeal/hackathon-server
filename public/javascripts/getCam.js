const cams = [
	{
		ip: 'camera1',
		rooms: [101, 102]
	},
	{
		ip: 'camera2',
		rooms: [103, 104, 105]
	},
	{
		ip: 'camera3',
		rooms: [106]
	}
];

module.exports = function findCam(room) {
	for (const cam of cams) {
		if (cam.rooms.includes(room)) return cam.ip;
	}
	return 'Out of range';
};
