const proximities = {
	Unknown: 0,
	Immediate: 1,
	Near: 2,
	Far: 3
};

module.exports = function closestBeacon(beacons) {
	let lowestDist = 50;
	let room = 0;

	for (const beacon of beacons) {
		if (beacon[1].distance < lowestDist) {
			lowestDist = beacon[1].distance;
			room = beacon[1].minor;
		}
	}

	return room;
};
