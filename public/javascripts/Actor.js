const getRoom = require('./getRoom');
const getCam = require('./getCam');

const actors = new Map();

module.exports = class Actor {
	constructor(ip, data) {
		this.ip = ip;
		this.name = `actor${actors.size + 1}`;
		this.room = getRoom(data);
		this.camera = getCam(this.room);

		actors.set(this.ip, this);
	}

	update(data) {
		this.room = getRoom(data);
		this.camera = getCam(this.room);

		actors.set(this.ip, this);
	}

	static actorExists(ip) {
		return actors.has(ip);
	}

	static getActor(ip) {
		return actors.get(ip);
	}
};
